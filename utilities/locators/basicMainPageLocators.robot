*** Variables ***
${QUOTE_BTN}   css=.expand-btn .btn-cta-text
${QUOTE_FULL_NAME}    //form[@id='adt-hfe-form-id']//input[contains(@id,'Full_Name')]
${QUOTE_EMAIL}    //form[@id='adt-hfe-form-id']//input[contains(@id,'Mail')]
${QUOTE_PHONE}    //form[@id='adt-hfe-form-id']//input[contains(@id,'Phone')]
${QUOTE_POSTAL}    //form[@id='adt-hfe-form-id']//input[contains(@id,'Postal')]
${PACKAGE_LINK}    //a[@title='Packages'][@data-position='desktop']

package automation.adt;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GetAFreeQuoteTest {
	WebDriver driver;
	@Test 
	public void testaMethod() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/Users/schippa/Documents/Projects/KM/knowledge-ct/testautomation-robotframework/drivers/chromedriver");
		driver = new ChromeDriver();
		//driver.manage().window().setSize(new Dimension(360, 640));
		// String url = "https://www.adt.com/";
		String url = "https://adt-stage-63.adobecqms.net/"; 

		driver.get(url);
		AssertJUnit.assertEquals(driver.getTitle(), "ADT® Security Alarm Systems for Home & Business");
		driver.findElement(By.xpath("//*[@id=\"expand-btn\"]/div/div/a")).click();
		// ExpectedConditions.presenceOfElementLocated(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Full_Name')]")).wait(100);
		// WebDriverWait.until(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Full_Name')]"));
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		driver.findElement(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Full_Name')]")).click();
		//System.out.println(driver.findElement(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Full_Name')]")).getClass());
		driver.findElement(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Full_Name')]")).sendKeys("Test First Name");
		driver.findElement(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Mail')]")).sendKeys("test@test.com");
		driver.findElement(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Phone')]")).sendKeys("09999");
		driver.findElement(By.xpath("//form[@id='adt-hfe-form-id']//input[contains(@id,'Postal')]")).sendKeys("503503");
		//System.out.println("Suddu" + driver.findElement(By.cssSelector("#hfe-phone .input-error-txt")).getText());
		Assert.assertEquals(driver.findElement(By.cssSelector("#hfe-phone .input-error-txt")).getText(), "Enter a valid phone number");
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("**** Before Method");
	}
	@AfterMethod
	public void afterMethod() {
		System.out.println("**** Closing browser");
//		driver.quit();
	}
}

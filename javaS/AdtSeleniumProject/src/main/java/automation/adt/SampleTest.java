package automation.adt;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
public class SampleTest {
	WebDriver driver;

	@Test 
	public void testaMethod() {
		System.setProperty("webdriver.chrome.driver", "/Users/schippa/Documents/Projects/KM/knowledge-ct/testautomation-robotframework/drivers/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().setSize(new Dimension(360, 640));
		String url = "https://adt-stage-63.adobecqms.net/"; //https://www.adt.com/";
		driver.get(url);
		Assert.assertEquals(driver.getTitle(), "ADT® Security Alarm Systems for Home & Business");
		driver.findElement(By.linkText("Specials")).click();
		Assert.assertEquals(driver.getTitle(), "ADT® | Our Best Specials and Deals on Home Security Systems 2020");
		driver.findElement(By.linkText("Packages")).click();
		Assert.assertEquals(driver.getTitle(), "Compare ADT® Home Security Packages (2021)");
		//driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
		
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("**** Before Method");
	}
	@AfterMethod
	public void afterMethod() {
		System.out.println("**** Closing browser");
		//driver.quit();
	}
	
}
